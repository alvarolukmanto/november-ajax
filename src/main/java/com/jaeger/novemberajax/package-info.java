/**
 * <code>november-ajax</code> is various technical tools to share, that needs
 * high performance but simple functionalities:
 * <ul>
 * <li>notifier via email</li>
 * <li>notifier via sms</li>
 * <li>generic pubsub json publisher</li>
 * <li>QR code generator</li>
 * </ul>
 * 
 * @author timpamungkas
 *
 */
package com.jaeger.novemberajax;
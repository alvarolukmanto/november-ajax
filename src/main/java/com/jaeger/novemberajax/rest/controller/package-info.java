/**
 * Spring REST controllers.
 * 
 * @author timpamungkas
 *
 */
package com.jaeger.novemberajax.rest.controller;
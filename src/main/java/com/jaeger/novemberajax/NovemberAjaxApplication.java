package com.jaeger.novemberajax;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NovemberAjaxApplication implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(NovemberAjaxApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(NovemberAjaxApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info(NovemberAjaxApplication.class.getSimpleName() + " started");
	}

}

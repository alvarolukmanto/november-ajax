package com.jaeger.novemberajax.service;

import java.io.IOException;

public interface QrCodeGenerator {

	public enum ImageFormat {
		PNG, JPG, JPEG
	}

	byte[] getQrCode(String text, int size, ImageFormat imageFormat) throws IOException;

}

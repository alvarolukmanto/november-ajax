package com.jaeger.novemberajax;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectPackages(value = { "com.jaeger.novemberajax.rabbitmq", "com.jaeger.novemberajax.rest",
		"com.jaeger.novemberajax.util" })
public class AllTests {

}

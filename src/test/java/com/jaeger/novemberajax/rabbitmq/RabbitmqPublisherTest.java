package com.jaeger.novemberajax.rabbitmq;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.amqp.AmqpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jaeger.novemberajax.service.impl.RabbitmqPublisher;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class RabbitmqPublisherTest {

	class Car {
		private String brand;
		private String color;
		private double price;

		Car(String brand, String color, double price) {
			super();
			this.brand = brand;
			this.color = color;
			this.price = price;
		}

		public String getBrand() {
			return brand;
		}

		public String getColor() {
			return color;
		}

		public double getPrice() {
			return price;
		}

		public void setBrand(String brand) {
			this.brand = brand;
		}

		public void setColor(String color) {
			this.color = color;
		}

		public void setPrice(double price) {
			this.price = price;
		}
	}

	private static final String EXCHANGE_TEST = "x.pork.work";
	private static final String ROUTING_KEY_TEST = "q.pork.fried";

	@Autowired
	private RabbitmqPublisher rabbitmqPublisher;

	@Test
	public final void testPublishAnyString() {
		try {
			assertTrue(rabbitmqPublisher.publish(EXCHANGE_TEST, ROUTING_KEY_TEST, "pork"));
		} catch (IOException e) {
			fail(e.getClass() + " thrown : " + e.getMessage());
		}
	}

	@Test
	public final void testPublishAsJsonWithInvalidJsonEntry() {
		var invalidJson = List.of("{this is absolutely not a valid json}", "{key\": \"value}",
				"{ \"color\" : \"Red\", \"type\" \"Ford\" }", "{\"key\":\"value\"",
				"{\"color : \"Silver\", \"type\" : \"Honda\"}", "{a: tail}", "[\"key\":\"value\"]");

		for (String s : invalidJson) {
			assertThrows(JsonParseException.class,
					() -> rabbitmqPublisher.publishAsJson(EXCHANGE_TEST, ROUTING_KEY_TEST, s));
		}
	}

	@Test
	public final void testPublishAsJsonWithValidJsonEntry() {
		try {
			String validJson = "{ \"color\" : \"Black\", \"type\" : \"BMW\" }";
			assertTrue(rabbitmqPublisher.publishAsJson(EXCHANGE_TEST, ROUTING_KEY_TEST, validJson));
			assertTrue(rabbitmqPublisher.publishAsJson(EXCHANGE_TEST, ROUTING_KEY_TEST,
					new Car("Toyota", "black", 200000)));

			var objectMapper = new ObjectMapper();
			var validJsonUrls = List.of(
					"https://gist.githubusercontent.com/planetoftheweb/8bc1698e423ef0ff3db734f4115bf214/raw/27d4811f120b784ca9e35182419cffea377439b7/data.json",
					"https://raw.githubusercontent.com/sitepoint-editors/json-examples/master/src/products.json",
					"https://raw.githubusercontent.com/sitepoint-editors/json-examples/master/src/youtube-search-results.json",
					"https://raw.githubusercontent.com/LearnWebCode/json-example/master/pets-data.json",
					"https://raw.githubusercontent.com/LearnWebCode/json-example/master/animals-1.json",
					"https://raw.githubusercontent.com/LearnWebCode/json-example/master/animals-2.json");

			for (var s : validJsonUrls) {
				JsonNode jsonNode = objectMapper.readTree(new URL(s));
				assertNotNull(jsonNode);
				rabbitmqPublisher.publishAsJson(EXCHANGE_TEST, ROUTING_KEY_TEST, jsonNode);
			}
		} catch (IOException e) {
			fail(e.getClass() + " thrown : " + e.getMessage());
		}
	}

	@Disabled
	@Test
	public final void testPublishInvalidExchange() {
		assertThrows(AmqpException.class, () -> {
			rabbitmqPublisher.publish(RandomStringUtils.randomAlphanumeric(10),
					RandomStringUtils.randomAlphanumeric(10), "message");
		});
	}

}

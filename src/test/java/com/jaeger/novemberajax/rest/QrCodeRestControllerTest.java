package com.jaeger.novemberajax.rest;

import static java.time.Duration.ofMillis;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.jaeger.novemberajax.rest.controller.QrCodeRestController;
import com.jaeger.novemberajax.service.QrCodeGenerator.ImageFormat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class QrCodeRestControllerTest {

	static MultiValueMap<String, String> params;

	static final String basicAuthString = "Basic " + Base64Utils.encodeToString("dev:password".getBytes());
	static final String ENDPOINT_GENERATE_QR_CODE = "/v1/qr";
	static final String PARAM_QR_TEXT = "t";
	static final String PARAM_QR_SIZE = "s";
	static final String PARAM_QR_FORMAT = "f";

	@BeforeAll
	static final void setup() {
		params = new LinkedMultiValueMap<>();
	}

	@Autowired
	private MockMvc mockMvc;

	@BeforeEach
	final void init() {
		params.clear();
	}

	@Test
	final void testGenerateQrCode_InvalidParams() throws Exception {
		var invalidFormats = List.of(" ", "JPGG", "pngg", "GIF", "SVG");
		var invalidTexts = List.of(RandomStringUtils.randomAlphanumeric(4296), RandomStringUtils.randomNumeric(7090));
		var invalidSizes = new ArrayList<Integer>();
		invalidSizes.add(QrCodeRestController.ALLOWABLE_SIZE_MAX + 1);
		invalidSizes.add(QrCodeRestController.ALLOWABLE_SIZE_MIN - 1);
		for (int i = 5; i < 10; i++) {
			invalidSizes.add(ThreadLocalRandom.current().nextInt(QrCodeRestController.ALLOWABLE_SIZE_MIN - 1000,
					QrCodeRestController.ALLOWABLE_SIZE_MIN - 1));
			invalidSizes.add(ThreadLocalRandom.current().nextInt(QrCodeRestController.ALLOWABLE_SIZE_MAX + 1,
					QrCodeRestController.ALLOWABLE_SIZE_MAX + 1000));
		}

		// invalid format
		for (var f : invalidFormats) {
			params.clear();
			params.add(PARAM_QR_FORMAT, f);
			params.add(PARAM_QR_SIZE,
					System.currentTimeMillis() % 2 == 0 ? Integer.toString(QrCodeRestController.ALLOWABLE_SIZE_MAX)
							: Integer.toString(QrCodeRestController.ALLOWABLE_SIZE_MIN));
			params.add(PARAM_QR_TEXT, RandomStringUtils.randomAlphanumeric(1, 200));

			final var requestBuilder = MockMvcRequestBuilders.get(ENDPOINT_GENERATE_QR_CODE).params(params);

			assertTimeout(ofMillis(500), () -> {
				mockMvc.perform(requestBuilder).andExpect(status().isBadRequest());
			});
		}

		// invalid size
		for (var s : invalidSizes) {
			params.clear();
			params.add(PARAM_QR_FORMAT,
					System.currentTimeMillis() % 2 == 0 ? ImageFormat.JPG.toString() : ImageFormat.PNG.toString());
			params.add(PARAM_QR_SIZE, Integer.toString(s));
			params.add(PARAM_QR_TEXT, RandomStringUtils.randomAlphanumeric(1, 200));

			final var requestBuilder = MockMvcRequestBuilders.get(ENDPOINT_GENERATE_QR_CODE).params(params);

			assertTimeout(ofMillis(500), () -> {
				mockMvc.perform(requestBuilder).andExpect(status().isBadRequest());
			});
		}

		// invalid text (too long) should return 500
		for (var t : invalidTexts) {
			params.clear();
			params.add(PARAM_QR_FORMAT,
					System.currentTimeMillis() % 2 == 0 ? ImageFormat.JPG.toString() : ImageFormat.PNG.toString());
			params.add(PARAM_QR_SIZE,
					System.currentTimeMillis() % 2 == 0 ? Integer.toString(QrCodeRestController.ALLOWABLE_SIZE_MAX)
							: Integer.toString(QrCodeRestController.ALLOWABLE_SIZE_MIN));
			params.add(PARAM_QR_TEXT, t);

			final var requestBuilder = MockMvcRequestBuilders.get(ENDPOINT_GENERATE_QR_CODE).params(params);

			assertTimeout(ofMillis(500), () -> {
				mockMvc.perform(requestBuilder).andExpect(status().isInternalServerError());
			});
		}
	}

	@Test
	final void testGenerateQrCode_ValidParams() throws Exception {
		var validFormats = List.of("", ImageFormat.PNG.toString(), ImageFormat.JPEG.toString(),
				ImageFormat.JPG.toString());

		var validTexts = List.of("Dummy Text", "http://www.detik.com", "geo:48,9937,9,32774,400",
				RandomStringUtils.randomAlphanumeric(1, 2000), RandomStringUtils.randomAlphanumeric(4296).toUpperCase(),
				RandomStringUtils.randomNumeric(1, 7089), RandomStringUtils.randomNumeric(7089));

		var validSizes = new ArrayList<Integer>();
		validSizes.add(QrCodeRestController.ALLOWABLE_SIZE_MAX);
		validSizes.add(QrCodeRestController.ALLOWABLE_SIZE_MIN);
		for (int i = 0; i < 10; i++) {
			validSizes.add(ThreadLocalRandom.current().nextInt(QrCodeRestController.ALLOWABLE_SIZE_MIN,
					QrCodeRestController.ALLOWABLE_SIZE_MAX));
		}

		for (var f : validFormats) {
			for (var t : validTexts) {
				for (var s : validSizes) {
					params.clear();
					params.add(PARAM_QR_FORMAT, f);
					params.add(PARAM_QR_SIZE, Integer.toString(s));
					params.add(PARAM_QR_TEXT, t);

					final var requestBuilder = MockMvcRequestBuilders.get(ENDPOINT_GENERATE_QR_CODE).params(params);

					assertTimeout(ofMillis(500), () -> {
						mockMvc.perform(requestBuilder).andExpect(status().isOk());
					});
				}
			}
		}
	}

}
